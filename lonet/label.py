"""
Label module.
"""

class LevelLabel:
    level = 0
    name = ""

def saveLabelSet(file, s):
    """Write a dict s of labels to the given file."""  
        
    localOpen = False
    
    if not hasattr(file, "write"):
        file = open(file, mode="w")
        localOpen = True
        
    for label in s:
        file.write("%s\n" % (str(label)) )
            
    if localOpen:
        file.close()
    
def loadLabelSet(file):
    """Return a dictonary of labels stored in file."""
    
    s = set()
    localOpen = False
    
    if not hasattr(file, "readlines"):
        file = open(file, mode="r")
        localOpen = True
        
    for line in file.readlines():
        label = line.strip()
        if not line: continue
        try:
            s.add(label)
        except ValueError:
            pass
    
    if localOpen: 
        file.close()
    
    return s


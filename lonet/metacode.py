class IMetacode(object):
    """Metacode Interface."""
    
    def preCall(self, op, path, *args, **kwarks):
        raise NotImplementedError
            
    def postCall(self, op, path, retval, *args, **kwargs):
        raise NotImplementedError

class mcCallLogger(IMetacode):    
    def __init__(self):
        self.count = 0

    def preCall(self, op, path, *args, **kwarks):
        self.count += 1
        return True

    def postCall(self, op, path, retval, *args, **kwargs):
        return retval


def loadMetaCode(file):
    """Load and initialize metacode from a briefcase"""
    
    localOpen = False
    
    if not hasattr(file, "readlines"):
        file = open(file, mode="r")
        localOpen = True

    o = yaml.load(file)

    if localOpen: 
        file.close()
    
    return o

def saveMetaCode(file, o):
    
    localOpen = False
    
    if not hasattr(file, "write"):
        file = open(file, mode="w")
        localOpen = True
        
    yaml.dump(o, file)
                    
    if localOpen:
        file.close()

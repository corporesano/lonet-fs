import logging, os.path

METATAG = ":"    

def metaOpen(path, attr, mode="r"):
    """Return a file object for accessing meta infomration for path."""

    metadir = metaDir(path)
           
    if not os.path.exists(metadir):
        os.mkdir(metadir, mode=0o770)
    elif not os.path.isdir(metadir):
        raise FileExistsError("Metadir exists as a file %s", metadir)

    metafile = metaJoin(metadir, attr)
    f = open(metafile, mode)
    os.chmod(metafile, mode=0o660)
    return f

def ismeta(path):
    pathB = path.rsplit(METATAG, 1)
    assert len(pathB) > 0       

    if len(pathB) == 1:
        # No meta seperator found.
        return False
    else:                                   
        return True

def metaExists(path, attr):
    path = os.path.normpath(path)        
    mpath = metaJoin(path, attr)
    return os.path.exists(mpath)
    
def metaDir(path):
    """Returns path of the metadirectory for the given path."""
    
    path = os.path.normpath(path)
    base, meta = metaSplit(path)        
    return base + METATAG
            
def metaJoin(base, attr):
    """Return the path for meta attribute attr for the file path string
    base. If attr = '', the metadir is returned.
    
    Raises ValueError if base is already a metapath.
    """
    base = os.path.normpath(base)
    
    if metaSplit(base)[1]: 
        raise ValueError("base is already a metapath %s" % base)
    
    attr = attr.strip(os.sep + METATAG + ".")
    if base != os.sep:
        base = base.rstrip(os.sep + METATAG + ".")
    
    if not attr:
        return base + METATAG 
    else:
        return base + METATAG + os.sep + attr

metapath = metaJoin

def metaSplit(path):
    """Returns a (base, attr) tuple refering to the filename path (base)
    and the meta attribute name (attr) encoded in the path string.
    
    Base never contains a trailing slash.
    
    Returns (base, '') if the path contains no meta-attribute. 
    """
    path = os.path.normpath(path)
    pathL = path.rsplit(METATAG, 1)

    basa = meta = None
    if len(pathL) == 1:
        base = pathL[0]
        meta = ''
    else:
        base, meta = pathL

    # Remove special characters in front
    meta = meta.strip(os.sep + METATAG + ". \t")
    if len(base) > 1:
        base = base.rstrip(os.sep + METATAG + ". \t")
            
    return (base, meta)


class IMetastore(object):
    """Interface class for Metastores."""
    
    def metaOpen(self, path, attr, mode="r"):
        raise NotImplementedError

    def metaExists(self, path, attr):
        raise NotImplementedError

    def ismeta(self, path):
        raise NotImplementedError
              
class SidecarMetastoreMixIn(object):
    """A mixin class that will store metatada in sidecar files."""

    log = logging.getLogger(name='SidecarMetaStoreMixIn')
    METATAG = ":"

    def __init__(self):
        self._meta = {}

    def isMetaFile(self, path, attr=""):        
        if not attr:
            return self.METATAG in os.path.basename(path)
        else:
            return bool(path.endswith(self.METATAG + attr))

    def toSidecarPath(self, path, attr):
        return path + self.METATAG + attr

    def loadMeta(self, path, attr):        

        if self.isMetaFile(path):
            raise FuseOSError(fuse.ENOENT)        

        sideCarFile = self.toSidecarPath(path, attr)

        if sideCarFile not in self._meta:
            if os.path.exists(sideCarFile):                        
                try:
                    with open(sideCarFile, "r") as file:
                        self._meta[sideCarFile] = yaml.load(file)      
                except Exception as e:
                    self.log.debug("Failed to load meta %s from '%s': %s", attr, sideCarFile, e)
                    raise FuseOSError(fuse.ENOENT)

        self.log.debug("loadMeta from %s: %s", sideCarFile, str(self._meta.get(sideCarFile, None)))                                                   
        return self._meta.get(sideCarFile, None)           


    def saveMeta(self, path, attr, data):

        if self.isMetaFile(path):
            raise FuseOSError(fuse.ENOENT)

        sideCarFile = self.toSidecarPath(path, attr)

        self._meta[sideCarFile] = data            
        self.log.debug("saveMeta to %s: %s", sideCarFile, data) 

        self.flushMeta(path, attr)

    def flushMeta(self, path, attr):

        if self.isMetaFile(path):
            raise FuseOSError(fuse.ENOENT)
        sideCarFile = self.toSidecarPath(path, attr)

        data = self._meta[sideCarFile]

        with open(sideCarFile, "w") as file:
            yaml.dump(data, file)
        self.log.debug("flushMeta to %s: %s", sideCarFile, yaml.dump(data))                    


class XATTRMetaStoreMixIn:
    """A mixin class that will store metadata in files' xattr fields.

    TODO: Make it work after recent changes. 
    """

    log = logging.getLogger(name='XATTRMetaStoreMixIn')
    XATTR_METACODE = "user.metacode"
    XATTR_TAINT = "user.taint"

    def toSourcePath(self, path):
        """Convert mountpoint paths, to filesystem paths."""
        raise NotImplementedError

    def loadTaint(self, path):
        # Load file taint     
        fileTaint = set()
        if os.path.exists(self.toSourcePath(path)) and XATTR_TAINT in os.listxattr(self.toSourcePath(path)):                        
            try:
                fileTaint = os.getxattr(self.toSourcePath(path), XATTR_TAINT)  
                fileTaint = pickle.loads(fileTaint)      
            except Exception as e:
                self.log.debug("Failed to load taint from file %s: %s", path, e)
                raise FuseOSError(fuse.ENOENT)
        return fileTaint

    def loadMetacode(self, path):
        # Load metacode        
        mcode = Metacode()        
        if os.path.exists(self.toSourcePath(path)) and XATTR_METACODE in os.listxattr(self.toSourcePath(path)):                        
            try:
                mcode = os.getxattr(self.toSourcePath(path), XATTR_METACODE)  
                mcode = pickle.loads(mcode)      
            except Exception as e:
                self.log.debug("Failed to load metacode on %s:%s", path, e)
                raise FuseOSError(fuse.ENOENT)
        return mcode

    def saveMetacode(self, path, mcode):
        # Persist metacode if file exists.
        if os.path.exists(self.toSourcePath(path)):
            os.setxattr(self.toSourcePath(path), XATTR_METACODE, pickle.dumps(mcode))

    def saveTaint(self, path, taint):
        # Persist fileTaint if file exists.
        if os.path.exists(self.toSourcePath(path)):
            os.setxattr(self.toSourcePath(path), XATTR_TAINT, pickle.dumps(taint))     

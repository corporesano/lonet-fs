import random

registry = {}

POLICYATTR = "policy"

def registerPolicyClass(cls):
    registry[cls.policyId] = cls


class BasicPolicy(object):
   
    policyId = "pol:9951967a-8618-4b04-80b2-ed12cc92ded5"
    version = 1
    
    states = ['2-private', '1-deidentified', '1-statistic', '0-public']
    transitions = [('anonymize','2-private', '1-deidentified'),
                   ('anonymize','1-statistic', '0-public'),
                   ('aggregate','2-private', '1-statistic'),
                   ('aggregate','1-deidentified', '0-public'),                                      
                   ]
                 
    def __init__(self):
        self._cur_state = self.states[0]

    @property
    def state(self):
        return self._cur_state
    
    def checkValidType(self, to_ttype):
        """Check if to_ttype can be applied to the policy, and return new state. 
        Returns None if transition is invalid."""
        for ttype, sstate, estade in self.transitions:
            if ttype == to_ttype and sstate == self._cur_state:
                return estade
        return False

    def applyTType(self, ttype):
        """Apply a transition type ttype to the machine. Returns new state."""
        estate = self.checkValidType(ttype)
        if estate:
            self._cur_state = estate
            return estate
        else:
            return self.state
     
    def __getstate__(self):
        return {'state':self.state}

    def __setstate__(self, data):
        self._cur_state = data['state']

registerPolicyClass(BasicPolicy)



def savePolicy(sm, file):
    
    localOpen = False
    
    if not hasattr(file, "write"):
        file = open(file, mode="w")
        localOpen = True

    ret = file.write("%s %s" % (sm.policyId, sm.state))    

    if localOpen: 
        file.close()
    
    return ret

def applyTType(label, state, ttype):
    """Apply ttype on policy encoded in label. Returns next state."""
    p = fromLabel(label, state)
    p.applyTType(ttype)
    return p.state
            
def fromLabel(policyId, state):
    cls = registry[policyId]
    p = cls.__new__(cls)
    p.__setstate__({'state':state})
    return p

def toLabel(p):
    return (p.policyId, p.state)
    
def loadPolicy(file):
    
    localOpen = False
    
    if not hasattr(file, "readlines"):
        file = open(file, mode="r")
        localOpen = True

    policyId, state = file.read().strip().split()

    if localOpen: 
        file.close()
    cls = registry[policyId]
    p = cls.__new__(cls)
    p.__setstate__({'state':state})

    return p
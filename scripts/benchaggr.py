import time
import os, sys
import scipy
from optparse import OptionParser
import hashlib

blockSize = 1024*16

if __name__ == "__main__":
    
    parser = OptionParser()    
    (options, args) = parser.parse_args()
       
    exprsizes = map(lambda x: int(x), args)
    exprsizes = [1, 50, 100, 200, 400, 600, 1000]
    
    print("# nrfiles time")
    for expr in exprsizes:
        digest = hashlib.sha256()
        tStart = time.time()
        
        for fname in range(expr):    
            with open("in-%d" %fname, "rb") as f:
                data = f.read(blockSize)
                while data:
                    digest.update(data)
                    data =f.read(blockSize)
                        
        with open("of", "wb") as f:
            f.write(digest.digest())

        tEnd = time.time()
        
    
        print("%d %f" % (expr, tEnd - tStart))
        
        sys.stderr.write(("%d %f\n" % (expr, tEnd - tStart)))
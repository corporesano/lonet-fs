import time
import os, sys
import scipy

from optparse import OptionParser

def doTest(fn, blockSize, reps=10):
    
    l = ""
    outfile = "testout-%s" % fn
    infile = str(fn)
    
    samples = []
    for i in range(reps):
        
        tStart = time.time()    
        with open(infile, "rb") as fi:
            with open(outfile, "wb") as fo:
                l = fi.read(blockSize)
                while l:
                    fo.write(l),
                    l = fi.read(blockSize)                
        tEnd = time.time()
        
        samples.append(tEnd-tStart)

    mean = scipy.mean(samples)
    std = scipy.std(samples)
        
    return blockSize, mean, std

k = 1024
M = 1024 * k

infile = "65536"

if __name__ == "__main__":
    
    parser = OptionParser()    
    (options, args) = parser.parse_args()
    
    testSizes = [64, 124, 256, 512, 1*k, 4*k,  8*k, 16*k, 32*k, 64*k]
    
    print("# blocksize (bytes) mean std. Infile = %d ", infile)
    for size in testSizes:
        fn, mean, std = doTest(infile, size)
        
        print("%d %f %f" % (fn, mean, std))
        sys.stderr.write("%d %f %f\n" % (fn, mean, std))
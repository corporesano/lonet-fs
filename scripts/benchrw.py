import time
import os, sys
import scipy

blockSize = 1024 * 32

def doTest(fn, reps=10):
    
    l = ""
    outfile = "testout-%d" % fn
    infile = str(fn)
    
    samples = []
    for i in range(reps):
        
        tStart = time.time()    
        with open(infile, "rb") as fi:
            with open(outfile, "wb") as fo:
                l = fi.read(blockSize)
                while l:
                    fo.write(l),
                    l = fi.read(blockSize)                
        tEnd = time.time()
        
        samples.append(tEnd-tStart)
    #os.unlink(outfile)
    mean = scipy.mean(samples)
    std = scipy.std(samples)
        
    return fn, mean, std

if __name__ == "__main__":
    
    testSizes = [1, 4, 16, 32, 64, 1024,  4096, 16384, 32768, 65536]
    
    print("# filesize mean std")
    for size in testSizes:
        fn, mean, std = doTest(size)
        
        print("%d %f %f" % (fn, mean, std))
        sys.stderr.write("%d %f %f\n" % (fn, mean, std))
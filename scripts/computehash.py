#!python

import hashlib, sys

m = hashlib.sha256()        
with open(sys.argv[1], "rb") as f:
    l = f.read(10000)
            
    while l:
        m.update(l)
        l = f.read(10000)                
      
print(sys.argv[1], m.hexdigest())
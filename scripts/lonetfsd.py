#!/usr/bin/env python

import sys
sys.path.append('../lonet/')

from lonet.policy import *
import errno, os, logging
import fuse, random, io
from sys import argv, exit
from threading import Lock
from fuse import FUSE, FuseOSError, Operations, fuse_get_context
import stat
import lonet
import lonet.metafile
import re
import configparser
import shutil
import binascii
import hashlib
from lonet.metafile import *
from lonet.label import *
import optparse
import runpy
import yaml

logging.basicConfig(level=logging.DEBUG, format="%(asctime)s %(message)s")

# Groups

SESSIONMAGIC="SES"
SESSIDSIZE=32
NULLCAP = "0"

TAINTATTR = 'taint'
CLEARANCEATTR = 'clearance'

class ProcessInfo:
    uid = -1
    gid = -1
    pid = -1
    exe_path = ''
    exe = ''
    args = []
    args_raw = ""
    options = None
    ttype = 'unknown'
    sesId = '0'
    taint = None
    clearance = []
    environ = {}    
            
            
def find_mount_point(path):
    while not os.path.ismount(path):
        path=os.path.dirname(path)
    return path

def os_get_context():
    """Replacement contex function for use before inint when fuse_get_context does not work. """
    return (os.getuid(), os.getgid(), os.getpid())

get_context_func = os_get_context

_hashfilecache = {} 
def hashFile(path):
    """Return the hash of a file. The hash is cached."""
    global _hashfilecache

    #print("hashfile enter", path)
    
    if path not in _hashfilecache:
        
        _hashfilecache[path] = "0x0"

        m = hashlib.sha256()            

        #print("hashfile do access", path)        
        if os.access(path, os.R_OK):

            #print("hashfile do open", path)
            with open(path, "rb") as f:
                l = f.read()
                while l:
                    m.update(l)
                    l = f.read()             

            #print("hashfile read done", path)
            
        else:
            #print("hashfile no access", path)            
            pass
            
        _hashfilecache[path] = m.hexdigest()                       

    #print("hashfile exit", path)
    
    return _hashfilecache[path]

def loadBriefcase(file):
    """the clearance attribute name for cap."""    
    return "%s%s-clearance" % (SESSIONMAGIC, cap)

def sesClearAttr(ses):
    """Return the clearance attribute name for session."""    
    return "%s%s-clearance" % (SESSIONMAGIC, ses)

def sesTaintAttr(ses):
    """Return the clearance attribute name for cap."""    
    return "%s%s-taint" % (SESSIONMAGIC, ses)

def sesFile(root):
    """Returns the path of the cap file used to store taint and clearance
    attributes for capabilities."""
    return os.path.join(root, "cap")

def ttypeFile(root):
    """Returns the path of the cap file used to store taint and clearance
    attributes for capabilities."""
    return os.path.join(root, "ttypes")
       
METACODEATTR = "mcode"
BRIEFCASEATTR = "briefcase"

# 
# Policy Decorators
#
def denymetapath(func):
    """Raise exception if path is a metapath."""
    def func_wrapper(self, path, *args, **kwargs):
        if ismeta(path):
            raise FuseOSError(errno.EPERM)        
        return func(self, path, *args, **kwargs)
    return func_wrapper    

def exemetacode(func):
    """Raise exception if path is a metapath."""
    def func_wrapper(self, path, *args, **kwargs):

        filetaint = self.filetaint.get(path, set())
        
        for f in filetaint:
            pol = self.getPolicy(f)

            if 'metacode' not in pol: continue
            if 'OnAccess' in pol['metacode']:
                mf = self.toSourcePath(pol['metacode']['Onaccess'])
              
                mc = {}
                mc['pInfo'] = pInfo
                mc['path'] = path
                mc['store'] = metaDir(path)
                mc['op'] = op
                mc['log'] = self.log
                mc['briefcase'] = {}
                
                if metaExists(path, "OnAccess"):
                    with metaOpen(path, "OnAccess", "r") as bcfile:
                        mc['briefcase'] = yaml.load(bcfile)
                
                mc = runpy.run_path(mf, mc, run_name="metacode")
                
                with metaOpen(sPath, "OnAccess", "w") as bcfile:
                    yaml.dump(mc['briefcase'], bcfile)
            

        return func(self, path, *args, **kwargs)
    return func_wrapper    



def trackread(func):
    def func_wrapper(self, path, *args, **kwargs):    
        
        val = func(self, path, *args, **kwargs)        
        pInfo = kwargs['pInfo']
        
        pInfo.taint.update(self.filetaint[path])

        return val
    return func_wrapper

def trackwrite(func):
    def func_wrapper(self, path, *args, **kwargs):
        val = func(self, path, *args, **kwargs)
        
        pInfo = kwargs['pInfo']
        #filetaint = set(self.filetaint.get(path, [])).union(pInfo.taint)
        #newtaint = set()
        ttype = pInfo.ttype
        
        self.filetaint[path].update(pInfo.taint)

        toDelete = set()
        toAdd = set()
        # Apply transition
        for itm in self.filetaint[path]:
            
            pol = self.getPolicy(itm)                        
            #isinstance(pol, configparser.ConfigParser)
            transitions = pol['transitions']
            
            if ttype in transitions:
                newvalue = transitions[ttype]
                toDelete.add(itm)
                toAdd.add(newvalue)                            
            else:
                pass
      
        self.filetaint[path].update(toAdd)
        self.filetaint[path].difference_update(toDelete)
    
        return val
    
    return func_wrapper


def clearedOnly(func):
    def func_wrapper(self, path, *args, **kwargs):
        pInfo = kwargs['pInfo']
        f = self.filetaint.get(path,[])
         
        # Apply transition
        for idx, itm in enumerate(f):
            pol = self.getPolicy(itm)
            
            isinstance(pol, configparser.ConfigParser)
            if pInfo.ttype in pol['transitions'].keys():
                # OK transition type. Allow access
                break
            
            roles = pol['permissions'].get('roles', '').strip().split()
            roles = map(lambda x: x.strip(), roles)
            roles = filter(lambda x: x, roles) # Remove empty entries
            roles = list(filter(lambda x: x not in pInfo.clearance, roles))
                    
            if roles:
                self.log.debug("file %s missing roles roles %s pid=%d", path, list(roles), pInfo.pid)                
                raise FuseOSError(errno.EACCES)    
        
        val = func(self, path, *args, **kwargs)
        return val
    return func_wrapper


#
# Main FUSE implementation
#
class GirjiFS(object):

    log = logging.getLogger('GirjiFS')
    LOGGEDOPS = ['read', 'write', 'open', 'flush', 'unlink', 'release']
    
    def __init__(self, root, mountpoint):
        self.mountpoint = os.path.realpath(mountpoint)
        self.root = os.path.realpath(root)
        #self.sessionDir = os.path.join(metaDir(self.root), "sessions")
                
        self.rwlock = Lock()
        self.tTypes = configparser.ConfigParser()
        self.pidTTypes = {}
        self.sessions = {'0':set()}
        self.filetaint = {}
        self.policies = {}
        self.clearance = []

        # Register lonet pid
        pinfo = ProcessInfo()
        pinfo.uid = os.getuid()
        pinfo.gid = os.getgid()
        pinfo.pid = os.getpid()
        pinfo.exe = "lonet"
        pinfo.exe_path = "lonet"
        pinfo.ttype = "lonet" 
        self.pidTTypes[pinfo.pid] = pinfo  
        
        self.log.info("lonet started pid=%d", pinfo.pid)
        
        super().__init__()
        
    def getPolicy(self, name):
        if name not in self.policies:
            pol = configparser.ConfigParser()
            pol.read(self.toSourcePath(name))
            if not pol.has_section('transitions'):
                pol.add_section('transitions')
            if not pol.has_section('permissions'):
                    pol.add_section('permissions')
                
            self.policies[name] = pol

        return self.policies[name]
        
    def sessionPath(self, sesId):
        return os.path.join(self.sessionDir, str(sesId))

    def sessionAttribute(self, sesId, attr):
        return os.path.join(self.sessionDir, str(sesId), attr)


    #
    # Mics helper functions
    #
    
    def permission(self, taintL):
               
        perm = 0b111111111
        
        for p in taintL:
            
            pol = self.getPolicy(p)
            perm = perm & int(pol['perm']['acl'], 0)
        
        return perm
    
    def toSourcePath(self, path):
        """Convert mount path point to corresponding path in source directory."""
        return self.root + path
    
    def getProcessContext(self):        
        
        uid, gid, pid = get_context_func()        
        
        if pid in self.pidTTypes:
            return self.pidTTypes[pid]
                
        pinfo = ProcessInfo()
        pinfo.uid, pinfo.gid, pinfo.pid = uid, gid, pid

        #Initialize with 0 session         
        pinfo.taint = self.sessions['0']
                       
        # Important to set the pinfo structure now to allow for reentrant calls.
        self.pidTTypes[pid] = pinfo

        pinfo.exe_path = os.path.realpath("/proc/%d/exe" % pid)

        # Read exe binary hash.
        fn = pinfo.exe_path
        if not os.access(fn, mode=os.R_OK):
            raise FuseOSError(errno.EACCES)
        pinfo.exe = hashFile(pinfo.exe_path)

        # Read exe command line
        fn = "/proc/%d/cmdline" % pid
        if not os.access(fn, mode=os.R_OK):
            raise fuse.EACCES       
        with open(fn, mode="r") as f:
            pinfo.args = f.read().strip("\0").replace("\0", " ") 
            
        # Read environment variable
        pinfo.environ = {}
        env = []
        
        fn = "/proc/%d/environ" % pid        
        self.log.debug("getProcessContext opening env for pid=%s", pid)
        if os.access(fn, mode=os.R_OK):
            #self.log.debug("getProcessContext env access ok for %d", pid)
            
            f = os.open(fn, os.O_RDONLY|os.O_NONBLOCK|os.O_NDELAY)

            #self.log.debug("getProcessContext env open OK for %d", pid)

            #self.log.debug("getProcessContext env LOCK test %s for %d", os.lockf(f, os.F_TEST, 0), pid)
            
            #with open(fn, mode='r') as f:
            #env = io.BytesIO()
            env = os.read(f, 1024*128)

                    
            #self.log.debug("getProcessContext env read OK bytes %d for %d",  len(env), pid)   
            env = env.strip(b"\0")
            env = env.split(b"\x00")            
                
        for itm in env:   
            n = itm.split(b"=")
            if len(n) != 2: continue
            n, v = n    
            pinfo.environ[n.decode()] = v.decode()
            
        pinfo.sesId = pinfo.environ.get('LONET_SID', "0")
        self.log.debug("found session id %s for pid=%d exe=%s", pinfo.sesId, pinfo.pid, pinfo.exe_path)
        
        pinfo.taint = self.sessions[pinfo.sesId]
        
        # Load clearance for SES
        pinfo.clearance = self.clearance
        
        # Look for a matching ttype.
        pinfo.ttype = 'unknown'
        for sectionName, v in self.tTypes.items():

            isinstance(v, configparser.SectionProxy)
            # Skip default section.
            if sectionName == 'DEFAULT':
                continue
            
            if 'exe' not in v:
                self.log.debug('Computing hash for path %s in [%s]', v['exe_path'], sectionName)
                v['exe'] = str(hashFile(v['exe_path']))
                          
            # Skip entry if not matching executahle
            if not pinfo.exe == v['exe']:
                continue

            # Try to match arguments
            if 'arg' in v:
                
                arg_type = v['arg_type'].strip()                
                arg = v['arg'].strip()
                                
                # options that we allow.
                options_match = v.get('options_match', '').strip().split()
                options_havearg = v.get('options_havearg', ''). strip().split()
                
                # get cmd arguments, skipping first cmd argument.
                exe_args = pinfo.args.split()[1:]
                exe_remaining = []
                                
                while len(exe_args) > 0:
                    #print('while loop, exe_args', exe_args)
                    if exe_args[0] in options_match:
                        if exe_args[0] in options_havearg:                            
                            assert len(exe_args) > 0
                            exe_args.pop(0)
                            #print('pops1 ', exe_args)
                        exe_args.pop(0)
                        #print('pops1 ', exe_args)
                        
                    else:
                        #print('found arg. breaking', exe_args)
                        break
                        
                # Still some arguments left, we might have a match
                if len(exe_args) > 0:
                    itm = exe_args.pop(0)
                    #print("conisering arg, pid", p, pinfo.pid)
                    if arg_type == 'path':
                        if os.path.samefile(arg, itm):
                            #Match found!
                            pinfo.ttype = v.get('ttype', 'unknown')
                            break
                    elif arg_type == 'regexp':
                        if re.match(arg, itm):
                            self.log.debug("exe match by regexp %s", itm)
                            pinfo.ttype = v['ttype']
                            break
                        
                    elif arg_type == 'hash':
                        self.log.warning("Hashed exe arguments DOES not work!")
                        #print("type is hash")
                        cwd = os.path.realpath('/proc/%d/cwd' % pinfo.pid)
                        itm = os.path.join(cwd, itm)
                        itm = os.path.realpath(itm)
                        #print("real argument path is", itm)                        
                        
                        #print('try to do the hash match thing hashfile read = args', itm, arg)
                        hf = hashFile(p)
                        if hf == arg:
                            #print("hashfile match, setting ttype=", v['ttype'])
                            pinfo.ttype =  v['ttype']
                            break
                        else:
                            #print("hashfile does not match", hf, arg)
                            continue
                    else:
                        self.log.warning('Config file contains unknown argtype %s', arg_type)
                        continue
                else:
                    self.log.debug('No arguments to match')
            
        return pinfo
                

    def __call__(self, op, path, *args):

        meta = call = mcode = fileTaint = None
        cap = NULLCAP
        sesTaint = None
        capClear = None
        pInfo = None
        
        # Check that the invoked operation is a valid one. 
        if not hasattr(self, op):
            raise FuseOSError(errno.EFAULT)

        sPath = self.toSourcePath(path)

        if sPath not in self.filetaint:
            self.filetaint[sPath] = set()
          
        pInfo = self.getProcessContext()            

        if op in self.LOGGEDOPS or 'all' in self.LOGGEDOPS:
            self.log.debug("%s %s by %s (hash=%s pid=%d type=%s)", op, path, pInfo.exe_path, pInfo.exe[-4:], pInfo.pid, pInfo.ttype)
                                                         
        # Do call
        opfunc = getattr(self, op)
                                            
        retval = opfunc(sPath, *args, pInfo=pInfo)
            
                                         
        return retval
    
    #
    # Filesystem methods below.
    #    
    def access(self, path, mode, **kw):        
        if not os.access(path, mode):
            raise FuseOSError(errno.EACCES)

    bmap = None

    def chmod(self, path, mode, **kw):
        raise FuseOSError(errno.EROFS)

    def chown(self, path, uid, gid, **kw):
        raise FuseOSError(fuse.EROFS)

    def create(self, path, mode, fi=None, **kw):

        pInfo = kw['pInfo']
        rv =  os.open(path, os.O_WRONLY | os.O_CREAT, mode)        
        os.chown(path, pInfo.uid, pInfo.gid)
        return rv

    def destroy(self, path, **kw):
        'Called on filesystem destruction. Path is always /'        

    def flush(self, path, fh, **kw):

        if self.filetaint.get(path, None):            
            with metaOpen(path, TAINTATTR, mode='w') as f:
                saveLabelSet(f, self.filetaint[path])                           
        return os.fsync(fh)

    def fsync(self, path, datasync, fh, **kw):
        
        #if path in self.filetaint:
        #    with metaOpen(sPath, TAINTATTR, mode='w') as f:
        #        saveLabelSet(f, self.filetaint[path])        
        
        return os.fsync(fh)

    @denymetapath
    def getattr(self, path, fh=None, **kw):
        
        st = os.lstat(path)
        attrs =  dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
                                                          'st_gid', 'st_mode', 
                                                          'st_mtime', 'st_nlink',
                                                          'st_size', 'st_uid'))        
        
        #if os.path.commonprefix([self.sessionDir, path]) == self.sessionDir:
        #    attrs['st_size'] = SESSIDSIZE    
        
        #if not os.path.isdir(path):
        #    if self.checkClearance(kw['pInfo'].clearance, kw['fileTaint']):
        #        attrs['st_mode'] = stat.S_IFREG |stat.S_IRUSR | stat.S_IRGRP | stat.S_IROTH
        #    else:
        #        attrs['st_mode'] = stat.S_IFREG 
        
        return attrs
    
    @denymetapath
    def getxattr(self, path, name, position=0, **kw):
        retval = b""
        if metaExists(path, name):
            with metaOpen(path, name) as f:
                retval = bytes( f.read(), "UTF-8" )
        return retval

    def init(self, path, **kw):
        '''
        Called on filesystem initialization. (Path is always /).
        '''
        
        #with open(os.path.join(self.root, "newsession"), "wb") as f:
        #    f.write(binascii.b2a_hex(os.urandom(15)))
        
        if not metaExists(self.root, "ttypes"):
            self.log.warning("No TTypes found.")
        else:            
            self.tTypes.read(metaJoin(self.root, "ttypes"))
            self.log.info("TTypes loaded: %d entries.", len(self.tTypes.keys()))
        
        if not metaExists(self.root, CLEARANCEATTR):
            self.clearance = []
        else:
            with metaOpen(self.root, CLEARANCEATTR, mode="r") as f:                
                roles = f.readline().strip().split()
                roles = map(lambda x: x.strip(), roles)
                self.clearance = list(roles)
        self.log.info("Clearance for %s", self.clearance)
                
        
        global get_context_func        
        get_context_func = fuse_get_context

                
    def link(self, target, source, **kw):
        return os.link(source, target)

    def listxattr(self, path, **kw):
        return []

    lock = None

    def mkdir(self, path, mode, **kw):        
        return os.mkdir(path, mode=mode)
        

    def mknod(self, path, mode, dev, **kw):
        raise FuseOSError(fuse.EROFS)
    
    #@exemetacode
    def open(self, path, flags, mode=0o777, dir_fd=None, **kw):
        '''
        When raw_fi is False (default case), open should return a numerical
        file handle.

        When raw_fi is True the signature of open becomes:
        open(self, path, fi)

        and the file handle should be set directly.
        '''
        
        rv = os.open(path, flags, mode=mode, dir_fd=dir_fd)
        if path not in self.filetaint:
            self.filetaint[path] = set()

        if metaExists(path, TAINTATTR):        
            with metaOpen(path, TAINTATTR, mode='r') as f: 
                self.filetaint[path].update(loadLabelSet(f))                
        return rv 

    def opendir(self, path, **kw):
        'Returns a numerical file handle.'
        if path not in self.filetaint:
            self.filetaint[path] = set()
        
        
        return 0

    @clearedOnly
    @trackread
    @exemetacode
    def read(self, path, size, offset, fh, **kw):               
        
        if path == os.path.join(self.root, "newsession"):            
            sid = binascii.b2a_hex(os.urandom(15))[0:min(size, 15)]
            #sid = sid.decode('utf8')
            self.sessions[sid.decode('utf8')] = set()                        
            return sid

        with self.rwlock:
            os.lseek(fh, offset, 0)
            return os.read(fh, size)

    def readdir(self, path, fh, **kw):

        # '.' and '..' are not included in os.listdir, so yield them here.
        yield '.'
        yield '..'
        for file in os.listdir(path):
            if not ismeta(file):
                yield file
                
    @clearedOnly
    @trackread
    @exemetacode
    def readlink(self, path, **kw):
        return os.readlink(path)

    def release(self, path, fh, **kw):
        #if path in self.filetaint:
        #    with metaOpen(path, TAINTATTR, mode='w') as f:
        #        saveLabelSet(f, self.filetaint[path])        
                
        return os.close(fh)    

    def releasedir(self, path, fh, **kw):
        return 0

    def removexattr(self, path, name, **kw):
        raise FuseOSError(fuse.ENOTSUP)

    def rename(self, old, new, **kw):
        return os.rename(old, self.root + new)

    def rmdir(self, path, **kw):
        os.rmdir(path)
        if metaExists(path, None):
            os.rmdir(metaDir(path))
        
    def setxattr(self, path, name, value, options, position=0):
        raise FuseOSError(fuse.ENOTSUP)

    def statfs(self, path, **kw):
        stv = os.statvfs(path)
        return dict((key, getattr(stv, key)) for key in ('f_bavail', 'f_bfree',
                                                         'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag',
                                                         'f_frsize', 'f_namemax'))

    def symlink(self, target, source, **kw):
        return os.symlink(source, target)

    def truncate(self, path, length, fh=None, **kw):
        with open(path, 'r+') as f:
            f.truncate(length)

    def unlink(self, path, **kw):        
        metadir = metaDir(path)
        if os.path.exists(metadir):
            os.chmod(metadir, 0o770)
            shutil.rmtree(metadir)
        return os.unlink(path)

    def utimens(self, path, times=None, **kw):
        'Times is a (atime, mtime) tuple. If None use current time.'
        return 0

    @trackwrite
    @exemetacode
    def write(self, path, data, offset, fh, **kw):
  
        with self.rwlock:
            os.lseek(fh, offset, 0)
            return os.write(fh, data)

if __name__ == '__main__':    
    if len(argv) != 3:
        print('usage: %s <root> <mountpoint>' % argv[0])
        exit(1)

    name, root, mountpoint = argv

    # Run FUSE.
    fuse = FUSE(GirjiFS(root, mountpoint), mountpoint, foreground=True)


import glob
import zipfile
import argparse
import shutil
import os
import tempfile

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument('dstfile')
    parser.add_argument('srcdir')
    
    args = parser.parse_args()
    
    
    with tempfile.NamedTemporaryFile(mode="wb") as ntf:
        
        shutil.make_archive(ntf.name, 'zip', root_dir=args.srcdir, verbose=1)
        print(ntf.name, args.srcdir)
        shutil.copy(ntf.name, args.dstfile)
    
    #with zipfile.ZipFile(parser.dstfile, "w") as zf:
    #    isinstance(zf, zipfile.ZipFile)
    #    zf.
        
"""
Convert Python's cProfile output from marshal version 4 to marshal version 2. This in order to display 
the profiledata in RunSnakeRun, which currently only support Python 2.7
"""

import marshal
from optparse import OptionParser

usage = """usage: %prog [options] infile outfile"""

if __name__ == "__main__":
      
    parser = OptionParser()    
    (options, args) = parser.parse_args()
       
    infile, outfile = args
           
    data = marshal.load(open(infile, 'rb'))
    x = marshal.dump(data, open(outfile, 'wb'), 2)
    print("written %d bytes" % x)
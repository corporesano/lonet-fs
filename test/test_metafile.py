import unittest
import lonet

from lonet.metafile import *

class DirectoryMetastorePathTests(unittest.TestCase):
                            
    def test_paths(self):
        self.assertTrue(ismeta("a/b:c"))
        self.assertTrue(ismeta("/a/b:/c"))
        self.assertTrue(ismeta("/a/b/:/c"))
        self.assertTrue(ismeta("/a/b/:c"))        
        self.assertTrue(ismeta("/:/c"))
        self.assertTrue(ismeta(":/c"))
        self.assertTrue(ismeta(".:/c"))
        self.assertTrue(ismeta(":"))
        self.assertTrue(ismeta("/a:/b/c"))    
        self.assertTrue(ismeta("/:a/b/c"))    
        self.assertTrue(ismeta("/a/:b/c")) 
        
        self.assertFalse(ismeta("a/b/c"))
        self.assertFalse(ismeta("/a/b/c"))
        self.assertFalse(ismeta(""))
        self.assertFalse(ismeta("."))
        self.assertFalse(ismeta(".."))
        self.assertFalse(ismeta("/"))

    def test_splits(self):        
        self.assertEqual(metaSplit("/a/b/c"), ("/a/b/c", ''))               
        self.assertEqual(metaSplit("/"), ("/", ''))
        self.assertEqual(metaSplit("."), (".", ''))
        self.assertEqual(metaSplit("a/."), ("a", ''))
        self.assertEqual(metaSplit("a/./"), ("a", ''))
        self.assertEqual(metaSplit("/a/b:/c"),("/a/b", 'c'))
        self.assertEqual(metaSplit("a/b:/c"),("a/b", 'c'))
        self.assertEqual(metaSplit("a/:/c"),("a", 'c'))
        self.assertEqual(metaSplit(":a"),("", 'a'))
        self.assertEqual(metaSplit("/:a"),("/", 'a'))
        self.assertEqual(metaSplit(":/a"),("", 'a'))
        self.assertEqual(metaSplit(".:/a"), (".", 'a'))
        self.assertEqual(metaSplit(".:/./a"), (".", 'a'))
        
    def test_joins(self):

        self.assertEqual(metaJoin("/a/b", 'c'), "/a/b:/c")
        self.assertEqual(metaJoin("/a/b", '/c'), "/a/b:/c")
        self.assertEqual(metaJoin("/a/b/", 'c'), "/a/b:/c")
        self.assertEqual(metaJoin("/a/b/", ''), "/a/b:")
        self.assertEqual(metaJoin("/a/b", ''), "/a/b:")
        self.assertEqual(metapath("", 'c'), ':/c')
        self.assertEqual(metapath("/", 'c'), '/:/c')
        self.assertEqual(metapath(".", 'c'), ':/c')

    def test_metadir(self):

        self.assertEqual(metaDir("/a/b:/c"), "/a/b:")
        self.assertEqual(metaDir("/a/b"), "/a/b:")
        self.assertEqual(metaDir("/a/b/"), "/a/b:")
        self.assertEqual(metaDir("/a/b:/"), "/a/b:")
        self.assertEqual(metaDir("/a/b:"), "/a/b:")
        self.assertEqual(metaDir("/."), "/:")
        self.assertEqual(metaDir("/a/b:/"), "/a/b:")
                
if __name__ == '__main__':
    unittest.main()
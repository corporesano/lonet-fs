import unittest
import lonet
import lonet.metafile
import io

from lonet.policy import *

class PolicyTests(unittest.TestCase):
                            
    def test_states(self):
        p = BasicPolicy()        
        self.assertEqual(p.state, '2-private')
        self.assertTrue(p.applyTType('aggregate'))
        self.assertEqual(p.state, '1-statistic')
        self.assertTrue(p.applyTType('anonymize'))
        self.assertEqual(p.state, '0-public')

        p = BasicPolicy()        
        self.assertEqual(p.state, '2-private')
        self.assertTrue(p.applyTType('anonymize'))
        self.assertEqual(p.state, '1-deidentified')
        self.assertTrue(p.applyTType('aggregate'))
        self.assertEqual(p.state, '0-public')
       
        
    def test_loadFromLabel(self):                
        p = fromLabel(BasicPolicy.policyId, BasicPolicy.states[0])
        self.assertEqual(p.state, BasicPolicy.states[0])

    def test_applyTType(self):
        self.assertEqual(applyTType(BasicPolicy.policyId, BasicPolicy.states[0], 'anonymize'), BasicPolicy.states[2])

                
if __name__ == '__main__':
    unittest.main()